<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/partials/footer.htm */
class __TwigTemplate_ae7e38d5751074459e95dbcf252b3b6c3e45add1ca64fa1dc333523051078926 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer id=\"footer\" class=\"style10\">
\t\t\t\t<!-- footer top -->
\t\t\t\t<div class=\"footer-top bg-dark-jungle\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <div class=\"holder2\">
                                    <div class=\"logo\"><a href=\"#\"><img src=\"images/logo.png\" height=\"49\" width=\"90\" alt=\"fekra\"></a></div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <a href=\"about-us.html\" class=\"btn btn-more\">Sell your product</a>
                                </div>
                            </div>
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- footer cent -->
\t\t\t\t<div class=\"bg-dark-jungle footer-cent\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12 col-sm-6 col-md-3 info-box column1\">
                                <!-- f contact info2 -->
                                <div class=\"f-contact-info2\">
                                    <h5>Contact Information</h5>
                                    <address><i class=\"fa fa-map-marker\"></i> 1422 1st St. Santa Rosa,t CA 94559. USA</address>
                                    <div class=\"mail-box2\">
                                        <span class=\"mail-box\"><i class=\"fa fa-envelope-o\"></i> <a href=\"mailto:&#097;&#100;&#109;&#105;&#110;&#064;&#101;&#045;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;\" class=\"email\"> &#097;&#100;&#109;&#105;&#110;&#064;&#101;&#045;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></span>
                                    </div>
                                    <div class=\"tel-holder\">
                                        <span class=\"tel-box\"><i class=\"fa fa-phone\"></i> <a href=\"tel:00201008431112\" class=\"tel\">002- 01008431112</a></span>
                                    </div>
                                    <div class=\"mail-box2\">
                                        <span class=\"mail-box\"><i class=\"fa fa-link\"></i> <a href=\"mailto:&#119;&#119;&#119;&#046;&#121;&#111;&#117;&#114;&#095;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;\" class=\"email\">&#119;&#119;&#119;&#046;&#121;&#111;&#117;&#114;&#095;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-12 col-sm-6 col-md-3 column2\">
                                <!-- f mailing form2 -->
                                <div class=\"f-mailing-form2\">
                                    <h5>Mailing List</h5>
                                    <div class=\"mailing-form2\">
                                        <label>Please enter your email address for our mailing list to keep your self our lastest updated.</label>
                                        <div class=\"form-col\">
                                            <!-- Begin MailChimp Signup Form -->
                                            <div id=\"mc_embed_signup\">
                                                <form action=\"//htmlbeans.us11.list-manage.com/subscribe/post?u=cb2d5a07fdf0d86c96f260674&amp;id=1103b14a3b\" method=\"post\" id=\"mc-embedded-subscribe-form\" name=\"mc-embedded-subscribe-form\" class=\"validate\" target=\"_blank\" novalidate>
                                                    <div id=\"mc_embed_signup_scroll\">
                                                        <div class=\"mc-field-group\">
                                                            <label for=\"mce-EMAIL\">Email Address </label>
                                                            <input type=\"email\" value=\"\" name=\"EMAIL\" class=\"required email\" id=\"mce-EMAIL\" placeholder=\"Email Address\">
                                                        </div>
                                                        <div id=\"mce-responses\" class=\"clear\">
                                                            <div class=\"response\" id=\"mce-error-response\" style=\"display:none\"></div>
                                                            <div class=\"response\" id=\"mce-success-response\" style=\"display:none\"></div>
                                                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                                        <div style=\"position: absolute; left: -5000px;\"><input type=\"text\" name=\"b_cb2d5a07fdf0d86c96f260674_1103b14a3b\" tabindex=\"-1\" value=\"\"></div>
                                                        <div class=\"clear\">
                                                            <button type=\"submit\" name=\"subscribe\" id=\"mc-embedded-subscribe\" class=\"btn button btn-submit\"><i class=\"fa fa-paper-plane\"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--End mc_embed_signup-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class=\"col-xs-12 col-sm-6\">
                                <!-- f flicker -->
                                <div class=\"f-flicker\">
                                    <h5>Our Location</h5>
                                    <div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t\t<iframe width=\"100%\" height=\"200px\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d158858.1823707257!2d-0.10159865000000001!3d51.52864165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2z2YTZhtiv2YbYjCDYp9mE2YXZhdmE2YPYqSDYp9mE2YXYqtit2K_YqQ!5e0!3m2!1sar!2seg!4v1411153208328\" class=\"iframe_map\"></iframe>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
                                </div>
                            </div>
                            
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- footer bottom -->
\t\t\t\t<div class=\"bg-shark footer-bottom\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <!-- bottom box1 -->
                                <div class=\"bottom-box1\">
                                    
                                    <span class=\"copyright\">&copy; 2017 <a href=\"#\">Multi Mega</a></span>
                                </div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</footer>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/partials/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer id=\"footer\" class=\"style10\">
\t\t\t\t<!-- footer top -->
\t\t\t\t<div class=\"footer-top bg-dark-jungle\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <div class=\"holder2\">
                                    <div class=\"logo\"><a href=\"#\"><img src=\"images/logo.png\" height=\"49\" width=\"90\" alt=\"fekra\"></a></div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <a href=\"about-us.html\" class=\"btn btn-more\">Sell your product</a>
                                </div>
                            </div>
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- footer cent -->
\t\t\t\t<div class=\"bg-dark-jungle footer-cent\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12 col-sm-6 col-md-3 info-box column1\">
                                <!-- f contact info2 -->
                                <div class=\"f-contact-info2\">
                                    <h5>Contact Information</h5>
                                    <address><i class=\"fa fa-map-marker\"></i> 1422 1st St. Santa Rosa,t CA 94559. USA</address>
                                    <div class=\"mail-box2\">
                                        <span class=\"mail-box\"><i class=\"fa fa-envelope-o\"></i> <a href=\"mailto:&#097;&#100;&#109;&#105;&#110;&#064;&#101;&#045;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;\" class=\"email\"> &#097;&#100;&#109;&#105;&#110;&#064;&#101;&#045;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></span>
                                    </div>
                                    <div class=\"tel-holder\">
                                        <span class=\"tel-box\"><i class=\"fa fa-phone\"></i> <a href=\"tel:00201008431112\" class=\"tel\">002- 01008431112</a></span>
                                    </div>
                                    <div class=\"mail-box2\">
                                        <span class=\"mail-box\"><i class=\"fa fa-link\"></i> <a href=\"mailto:&#119;&#119;&#119;&#046;&#121;&#111;&#117;&#114;&#095;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;\" class=\"email\">&#119;&#119;&#119;&#046;&#121;&#111;&#117;&#114;&#095;&#100;&#111;&#109;&#097;&#105;&#110;&#046;&#099;&#111;&#109;</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-12 col-sm-6 col-md-3 column2\">
                                <!-- f mailing form2 -->
                                <div class=\"f-mailing-form2\">
                                    <h5>Mailing List</h5>
                                    <div class=\"mailing-form2\">
                                        <label>Please enter your email address for our mailing list to keep your self our lastest updated.</label>
                                        <div class=\"form-col\">
                                            <!-- Begin MailChimp Signup Form -->
                                            <div id=\"mc_embed_signup\">
                                                <form action=\"//htmlbeans.us11.list-manage.com/subscribe/post?u=cb2d5a07fdf0d86c96f260674&amp;id=1103b14a3b\" method=\"post\" id=\"mc-embedded-subscribe-form\" name=\"mc-embedded-subscribe-form\" class=\"validate\" target=\"_blank\" novalidate>
                                                    <div id=\"mc_embed_signup_scroll\">
                                                        <div class=\"mc-field-group\">
                                                            <label for=\"mce-EMAIL\">Email Address </label>
                                                            <input type=\"email\" value=\"\" name=\"EMAIL\" class=\"required email\" id=\"mce-EMAIL\" placeholder=\"Email Address\">
                                                        </div>
                                                        <div id=\"mce-responses\" class=\"clear\">
                                                            <div class=\"response\" id=\"mce-error-response\" style=\"display:none\"></div>
                                                            <div class=\"response\" id=\"mce-success-response\" style=\"display:none\"></div>
                                                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                                        <div style=\"position: absolute; left: -5000px;\"><input type=\"text\" name=\"b_cb2d5a07fdf0d86c96f260674_1103b14a3b\" tabindex=\"-1\" value=\"\"></div>
                                                        <div class=\"clear\">
                                                            <button type=\"submit\" name=\"subscribe\" id=\"mc-embedded-subscribe\" class=\"btn button btn-submit\"><i class=\"fa fa-paper-plane\"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--End mc_embed_signup-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class=\"col-xs-12 col-sm-6\">
                                <!-- f flicker -->
                                <div class=\"f-flicker\">
                                    <h5>Our Location</h5>
                                    <div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t\t<iframe width=\"100%\" height=\"200px\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d158858.1823707257!2d-0.10159865000000001!3d51.52864165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2z2YTZhtiv2YbYjCDYp9mE2YXZhdmE2YPYqSDYp9mE2YXYqtit2K_YqQ!5e0!3m2!1sar!2seg!4v1411153208328\" class=\"iframe_map\"></iframe>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
                                </div>
                            </div>
                            
                        </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- footer bottom -->
\t\t\t\t<div class=\"bg-shark footer-bottom\">
\t\t\t\t\t<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <!-- bottom box1 -->
                                <div class=\"bottom-box1\">
                                    
                                    <span class=\"copyright\">&copy; 2017 <a href=\"#\">Multi Mega</a></span>
                                </div>
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</footer>", "E:\\xampp7\\htdocs\\october/themes/Global Technology/partials/footer.htm", "");
    }
}
