<?php

/* E:\xampp7\htdocs\october/themes/jumplink-viola/partials/about.htm */
class __TwigTemplate_64ca7263b1c8341e3a500dd8f958f3d415a1017f81492e16a75b043967ce7dd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"about\" data-target=\"#about\">
    <div class=\"container text-xs-center p-y-3\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 p-b-2\">
                        <img class=\"img-fluid img-thumbnail center-block\" src=\"";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/about.jpg");
        echo "\" alt=\"me\">
                    </div>
                </div>
                <h2 class=\"text-xs-center\">";
        // line 10
        echo twig_escape_filter($this->env, ($context["headertext"] ?? null), "html", null, true);
        echo "</h2>
                ";
        // line 11
        $context['__cms_content_params'] = [];
        echo $this->env->getExtension('CMS')->contentFunction("about/about.htm"        , $context['__cms_content_params']        );
        unset($context['__cms_content_params']);
        // line 12
        echo "            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/about.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 12,  37 => 11,  33 => 10,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"about\" data-target=\"#about\">
    <div class=\"container text-xs-center p-y-3\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 p-b-2\">
                        <img class=\"img-fluid img-thumbnail center-block\" src=\"{{'assets/images/about.jpg'|theme}}\" alt=\"me\">
                    </div>
                </div>
                <h2 class=\"text-xs-center\">{{headertext}}</h2>
                {% content 'about/about.htm' %}
            </div>
        </div>
    </div>
</div>", "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/about.htm", "");
    }
}
