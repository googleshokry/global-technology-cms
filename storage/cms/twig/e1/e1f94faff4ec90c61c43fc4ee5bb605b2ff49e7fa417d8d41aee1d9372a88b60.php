<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/partials/header.htm */
class __TwigTemplate_a1d2ee44b44b87be58450ddbe0ff8b9264d27b2261f76a105cd46ff5f1172892 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"header\" class=\"style8\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<!-- header top -->
\t\t\t\t\t\t<div class=\"col-xs-12 header-top\">
\t\t\t\t\t\t\t<!-- Number -->
\t\t\t\t\t\t\t<nav class=\"nav language-nav\">
                                                            <p> Contact us on: 011111111 </p>
\t\t\t\t\t\t\t</nav>
\t\t\t\t\t\t\t<!-- social -->
\t\t\t\t\t\t\t<nav class=\"top-nav\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-google-plus\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-behance\"></i></a>
\t\t\t\t\t\t\t\t
                                                        </ul>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</nav>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t<!-- page logo -->
\t\t\t\t\t\t\t<div class=\"logo\">
\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo.png");
        echo "\" alt=\"Fekra\" class=\"img-responsive w-logo\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 30
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/logo-2.png");
        echo "\" alt=\"Fekra\" class=\"img-responsive b-logo\">
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t<!-- icon list -->
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled icon-list\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\" class=\"search-opener opener-icons\"><i class=\"fa fa-search\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t";
        // line 38
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("staticMenu"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 39
        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- main navigation of the page -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</header>
\t\t\t<!-- search popup -->
\t\t\t<div class=\"search-popup win-height\">
\t\t\t\t<div class=\"holder\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close-btn\">close</a>
\t\t\t\t\t\t\t\t<form action=\"#\" class=\"search-form\">
\t\t\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t\t\t<input type=\"search\" placeholder=\"search...\" class=\"search\">
\t\t\t\t\t\t\t\t\t\t<button class=\"submit\"><i class=\"fa fa-search\"></i></button>
\t\t\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 39,  64 => 38,  53 => 30,  49 => 29,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header id=\"header\" class=\"style8\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<!-- header top -->
\t\t\t\t\t\t<div class=\"col-xs-12 header-top\">
\t\t\t\t\t\t\t<!-- Number -->
\t\t\t\t\t\t\t<nav class=\"nav language-nav\">
                                                            <p> Contact us on: 011111111 </p>
\t\t\t\t\t\t\t</nav>
\t\t\t\t\t\t\t<!-- social -->
\t\t\t\t\t\t\t<nav class=\"top-nav\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-google-plus\"></i></a>
\t\t\t\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-behance\"></i></a>
\t\t\t\t\t\t\t\t
                                                        </ul>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</nav>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t<!-- page logo -->
\t\t\t\t\t\t\t<div class=\"logo\">
\t\t\t\t\t\t\t\t<a href=\"#\">
\t\t\t\t\t\t\t\t\t<img src=\"{{'assets/images/logo.png'|theme}}\" alt=\"Fekra\" class=\"img-responsive w-logo\">
\t\t\t\t\t\t\t\t\t<img src=\"{{'assets/images/logo-2.png'|theme}}\" alt=\"Fekra\" class=\"img-responsive b-logo\">
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t<!-- icon list -->
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled icon-list\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\" class=\"search-opener opener-icons\"><i class=\"fa fa-search\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t{% component 'staticMenu' %}
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<!-- main navigation of the page -->
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</header>
\t\t\t<!-- search popup -->
\t\t\t<div class=\"search-popup win-height\">
\t\t\t\t<div class=\"holder\">
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t<a href=\"#\" class=\"close-btn\">close</a>
\t\t\t\t\t\t\t\t<form action=\"#\" class=\"search-form\">
\t\t\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t\t\t<input type=\"search\" placeholder=\"search...\" class=\"search\">
\t\t\t\t\t\t\t\t\t\t<button class=\"submit\"><i class=\"fa fa-search\"></i></button>
\t\t\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>", "E:\\xampp7\\htdocs\\october/themes/Global Technology/partials/header.htm", "");
    }
}
