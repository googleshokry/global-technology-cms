<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/pages/singleproduct.htm */
class __TwigTemplate_6db2a6106ee362c2b207225a7fcac14930b5db2f33a370e1ca2a72a8efc11925 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">Product</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                              <section class=\"contact-block shop container\">
\t\t\t\t\t<div class=\"row shop-description\">
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\">
\t\t\t\t\t\t\t<!-- beans-stepslider2 -->
\t\t\t\t\t\t\t<div class=\"beans-stepslider2 description\">
\t\t\t\t\t\t\t\t<div class=\"beans-mask\">
\t\t\t\t\t\t\t\t\t<div class=\"beans-slideset\">
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"beans-pagination\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-inline\">
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 description-block\">
\t\t\t\t\t\t\t<header class=\"description-header\">
\t\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h2>Product Name</h2>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<p>Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. </p>
\t\t\t\t\t\t\t<p>Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<ul class=\"list-inline footer-social\">
\t\t\t\t\t\t\t\t<li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\"><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\"><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"behance\"><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                                  
                                  
                                  
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>
\t\t\t
\t\t\t";
        // line 80
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("footer"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/singleproduct.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 80,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">Product</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                              <section class=\"contact-block shop container\">
\t\t\t\t\t<div class=\"row shop-description\">
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\">
\t\t\t\t\t\t\t<!-- beans-stepslider2 -->
\t\t\t\t\t\t\t<div class=\"beans-stepslider2 description\">
\t\t\t\t\t\t\t\t<div class=\"beans-mask\">
\t\t\t\t\t\t\t\t\t<div class=\"beans-slideset\">
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<!-- beans-slide -->
\t\t\t\t\t\t\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/570x510\" >
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"beans-pagination\">
\t\t\t\t\t\t\t\t\t<ul class=\"list-inline\">
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t\t<li><img class=\"img-responsive\" alt=\"image description\" src=\"http://placehold.it/125x145\" ></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 description-block\">
\t\t\t\t\t\t\t<header class=\"description-header\">
\t\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h2>Product Name</h2>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<p>Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. </p>
\t\t\t\t\t\t\t<p>Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<ul class=\"list-inline footer-social\">
\t\t\t\t\t\t\t\t<li class=\"facebook\"><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"twitter\"><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"google-plus\"><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"behance\"><a href=\"#\"><i class=\"fa fa-behance\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                                  
                                  
                                  
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>
\t\t\t
\t\t\t{% partial \"footer\" %}", "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/singleproduct.htm", "");
    }
}
