<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/pages/about.htm */
class __TwigTemplate_f7a719cc44cc8678cb8fbd5470a910166390280314e19b11dd048a3b850fc042 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">About us</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                                <section class=\"container padding-top-60 padding-bottom-30\">
                                    
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"200\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>What We Do</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>Our Plans</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"400\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>Our Strategy</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/about.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">About us</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                                <section class=\"container padding-top-60 padding-bottom-30\">
                                    
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"200\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>What We Do</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>Our Plans</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t\t<!-- about-post -->
\t\t\t\t\t\t<article class=\"about-post col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"400\">
\t\t\t\t\t\t\t<div class=\"img-box\">
\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/370x270\" alt=\"image description\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<h3>Our Strategy</h3>
\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t</article>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>", "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/about.htm", "");
    }
}
