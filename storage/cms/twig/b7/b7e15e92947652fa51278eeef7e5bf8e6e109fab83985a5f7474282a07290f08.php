<?php

/* E:\xampp7\htdocs\october/plugins/rainlab/pages/components/staticmenu/default.htm */
class __TwigTemplate_d413a3ce434aabd6259e53f37b537b03169a685009687246eb10b39897b76e66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute(($context["__SELF__"] ?? null), "menuItems", array())) {
            // line 2
            echo "    <nav id=\"nav\">
\t
\t<div class=\"nav-holder\">
\t\t\t\t\t\t\t\t\t\t<ul class=\"list-inline nav-top\">
        ";
            // line 6
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['items'] = $this->getAttribute(($context["__SELF__"] ?? null), "menuItems", array())            ;
            echo $this->env->getExtension('CMS')->partialFunction((($context["__SELF__"] ?? null) . "::items")            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
            // line 7
            echo "\t\t</ul>
\t\t</div>
    </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/plugins/rainlab/pages/components/staticmenu/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 7,  27 => 6,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if __SELF__.menuItems %}
    <nav id=\"nav\">
\t
\t<div class=\"nav-holder\">
\t\t\t\t\t\t\t\t\t\t<ul class=\"list-inline nav-top\">
        {% partial __SELF__ ~ \"::items\" items=__SELF__.menuItems %}
\t\t</ul>
\t\t</div>
    </nav>
{% endif %}", "E:\\xampp7\\htdocs\\october/plugins/rainlab/pages/components/staticmenu/default.htm", "");
    }
}
