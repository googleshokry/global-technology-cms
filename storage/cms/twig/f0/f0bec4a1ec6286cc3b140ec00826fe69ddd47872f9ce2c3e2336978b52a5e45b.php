<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/layouts/main.htm */
class __TwigTemplate_7dd364e9712f7affd4ae303a849b1528d9e998242fafb3d5c283a16b96c98304 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

    <title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "site_name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
    <meta name=\"title\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">

    <!-- set the encoding of your site -->
    <meta charset=\"utf-8\">
    <!-- set the viewport width and initial-scale on mobile devices -->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <!-- set the title of your site -->
    <title>Global Technology</title>
    <!-- ========= Favicon Icons ========= -->
    <link rel=\"shortcut icon\" href=\"images/favicon/favicon.ico\">
    <!-- Standard iPhone Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"images/favicon/apple-touch-icon-57x57.png\">
    <!-- Retina iPhone Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"images/favicon/apple-touch-icon-114x114.png\">
    <!-- Standard iPad Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"images/favicon/apple-touch-icon-72x72.png\">
    <!-- Retina iPad Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"images/favicon/apple-touch-icon-144x144.png\">
    <!-- include Google fonts  -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic%7CPlayfair+Display:400,400italic,700,700italic,900,900italic%7CRoboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900%7CRaleway:400,100,200,300,500,600,700,800,900%7CGreat+Vibes%7CPoppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
     ";
        // line 27
        echo $this->env->getExtension('CMS')->assetsFunction('css');
        echo $this->env->getExtension('CMS')->displayBlock('styles');
        // line 28
        echo "        
    <link href=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(array(0 => "assets/css/bootstrap.css", 1 => "assets/css/font-awesome.min.css", 2 => "assets/css/page-assets.css", 3 => "assets/css/helper-elements.css", 4 => "assets/style.css", 5 => "assets/css/color/color.css", 6 => "assets/css/animate.css"));
        // line 37
        echo "\" rel=\"stylesheet\">
</head>
<body>

<!-- main container of all the page elements -->
<div id=\"wrapper\">
    <div class=\"w1\">
        <!-- header of the page style2 -->
     ";
        // line 45
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("header"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 46
        echo "        <!--code Content-->
        ";
        // line 48
        echo "        <section id=\"layout-content\">
            ";
        // line 49
        echo $this->env->getExtension('CMS')->pageFunction();
        // line 50
        echo "        </section>

        <!-- include jQuery library -->
        </div>
\t\t<div class=\"fa fa-chevron-up\" id=\"gotoTop\" style=\"display: none;\"></div>
\t</div>
            ";
        // line 56
        echo $this->env->getExtension('CMS')->assetsFunction('js');
        echo $this->env->getExtension('CMS')->displayBlock('scripts');
        // line 57
        echo " <script type=\"text/javascript\" src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(array(0 => "assets/js/jquery-1.11.3.min.js", 1 => "assets/js/jquery.main.js", 2 => "assets/js/plugins.js", 3 => "https://maps.googleapis.com/maps/api/js"));
        // line 61
        echo "\" rel=\"stylesheet\"></script>
    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/layouts/main.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 61,  99 => 57,  96 => 56,  88 => 50,  86 => 49,  83 => 48,  80 => 46,  76 => 45,  66 => 37,  64 => 29,  61 => 28,  58 => 27,  35 => 7,  31 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>

    <title>{{ this.theme.site_name }} - {{ this.page.title }}</title>
    <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
    <meta name=\"title\" content=\"{{ this.page.meta_title }}\">

    <!-- set the encoding of your site -->
    <meta charset=\"utf-8\">
    <!-- set the viewport width and initial-scale on mobile devices -->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <!-- set the title of your site -->
    <title>Global Technology</title>
    <!-- ========= Favicon Icons ========= -->
    <link rel=\"shortcut icon\" href=\"images/favicon/favicon.ico\">
    <!-- Standard iPhone Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"images/favicon/apple-touch-icon-57x57.png\">
    <!-- Retina iPhone Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"images/favicon/apple-touch-icon-114x114.png\">
    <!-- Standard iPad Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"images/favicon/apple-touch-icon-72x72.png\">
    <!-- Retina iPad Touch Icon-->
    <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"images/favicon/apple-touch-icon-144x144.png\">
    <!-- include Google fonts  -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic%7CPlayfair+Display:400,400italic,700,700italic,900,900italic%7CRoboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900%7CRaleway:400,100,200,300,500,600,700,800,900%7CGreat+Vibes%7CPoppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
     {% styles %}
        
    <link href=\"{{ ['assets/css/bootstrap.css',
    'assets/css/font-awesome.min.css',
    'assets/css/page-assets.css',
    'assets/css/helper-elements.css',
    'assets/style.css',
    'assets/css/color/color.css',
    'assets/css/animate.css',
    
    ]|theme }}\" rel=\"stylesheet\">
</head>
<body>

<!-- main container of all the page elements -->
<div id=\"wrapper\">
    <div class=\"w1\">
        <!-- header of the page style2 -->
     {% partial \"header\" %}
        <!--code Content-->
        {# Content #}
        <section id=\"layout-content\">
            {% page %}
        </section>

        <!-- include jQuery library -->
        </div>
\t\t<div class=\"fa fa-chevron-up\" id=\"gotoTop\" style=\"display: none;\"></div>
\t</div>
            {% scripts %}
 <script type=\"text/javascript\" src=\"{{ ['assets/js/jquery-1.11.3.min.js',
    'assets/js/jquery.main.js',
    'assets/js/plugins.js',
    'https://maps.googleapis.com/maps/api/js',    
    ]|theme }}\" rel=\"stylesheet\"></script>
    
    </body>
</html>", "E:\\xampp7\\htdocs\\october/themes/Global Technology/layouts/main.htm", "");
    }
}
