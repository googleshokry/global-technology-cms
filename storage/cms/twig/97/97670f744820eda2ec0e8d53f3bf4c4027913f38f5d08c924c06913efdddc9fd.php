<?php

/* E:\xampp7\htdocs\october/themes/jumplink-viola/partials/top_header.htm */
class __TwigTemplate_9b5784dd310d0b81b0885a09ce4da7b874e1a394066dc43cc201833296653f82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"top\" data-target=\"#top\">
    ";
        // line 2
        if ((($context["slideshow"] ?? null) && $this->getAttribute(($context["slideshow"] ?? null), "slideshow", array()))) {
            // line 3
            echo "        ";
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['slideshow'] = $this->getAttribute(($context["slideshow"] ?? null), "slideshow", array())            ;
            $context['__cms_partial_params']['id'] = "carousel-top-header"            ;
            echo $this->env->getExtension('CMS')->partialFunction("slideshow"            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
            // line 4
            echo "        <a class=\"icon-circle center-block\" href=\"";
            if (($this->getAttribute($this->getAttribute(($context["this"] ?? null), "page", array()), "id", array()) != "home")) {
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
            }
            echo "#about\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Weiter\">
            <i class=\"fa fa-chevron-circle-down\"></i>
        </a>
    ";
        }
        // line 8
        echo "</header>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/top_header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 8,  31 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header id=\"top\" data-target=\"#top\">
    {% if slideshow and slideshow.slideshow %}
        {% partial 'slideshow' slideshow=slideshow.slideshow id='carousel-top-header' %}
        <a class=\"icon-circle center-block\" href=\"{% if this.page.id != 'home' %}{{'home'|page}}{% endif %}#about\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Weiter\">
            <i class=\"fa fa-chevron-circle-down\"></i>
        </a>
    {% endif %}
</header>", "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/top_header.htm", "");
    }
}
