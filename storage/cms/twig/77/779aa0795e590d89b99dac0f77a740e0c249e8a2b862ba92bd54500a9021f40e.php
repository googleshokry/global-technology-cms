<?php

/* E:\xampp7\htdocs\october/themes/jumplink-viola/partials/services.htm */
class __TwigTemplate_70738e86e3198891d556ed7ad3bce6593d2fb9aa47e442c06dcc551246ad8a59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"bg-gray-lighter\" id=\"services\" data-target=\"#services\">
    <div class=\"container text-xs-center p-y-3\">
        <div class=\"row\">
            <div class=\"col-xs-12 text-xs-center\">
                <h2 class=\"m-b-1\">";
        // line 5
        echo twig_escape_filter($this->env, ($context["headertext"] ?? null), "html", null, true);
        echo "</h2>
                <p>";
        // line 6
        $context['__cms_content_params'] = [];
        echo $this->env->getExtension('CMS')->contentFunction("services/description.htm"        , $context['__cms_content_params']        );
        unset($context['__cms_content_params']);
        echo "</p>
            </div>
            <div class=\"col-xs-12\">
                ";
        // line 9
        if ((($context["slideshow"] ?? null) && $this->getAttribute(($context["slideshow"] ?? null), "slideshow", array()))) {
            // line 10
            echo "                    ";
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['slideshow'] = $this->getAttribute(($context["slideshow"] ?? null), "slideshow", array())            ;
            $context['__cms_partial_params']['id'] = "slideshow2"            ;
            echo $this->env->getExtension('CMS')->partialFunction("slideshow"            , $context['__cms_partial_params']            );
            unset($context['__cms_partial_params']);
            // line 11
            echo "                ";
        }
        // line 12
        echo "            </div>
            
            ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 15
            echo "                <div class=\"col-xs-12 col-md-4 p-t-2\">
                    ";
            // line 16
            $context["icon"] = (((("assets/images/icons/" . $this->getAttribute($this->getAttribute(($context["this"] ?? null), "theme", array()), "color", array())) . "/service_block_0") . $context["i"]) . ".svg");
            // line 17
            echo "                    <img class=\"center-block p-b-2\" src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(($context["icon"] ?? null));
            echo "\">
                    
                    ";
            // line 19
            if (($context["i"] == 1)) {
                // line 20
                echo "                        <h3 class=\"text-xs-center\">";
                echo twig_escape_filter($this->env, ($context["headertextblock1"] ?? null), "html", null, true);
                echo "</h3>
                    ";
            } elseif ((            // line 21
$context["i"] == 2)) {
                // line 22
                echo "                        <h3 class=\"text-xs-center\">";
                echo twig_escape_filter($this->env, ($context["headertextblock2"] ?? null), "html", null, true);
                echo "</h3>
                    ";
            } elseif ((            // line 23
$context["i"] == 3)) {
                // line 24
                echo "                        <h3 class=\"text-xs-center\">";
                echo twig_escape_filter($this->env, ($context["headertextblock3"] ?? null), "html", null, true);
                echo "</h3>
                    ";
            }
            // line 26
            echo "                    
                    ";
            // line 27
            $context["content"] = (("services/block" . $context["i"]) . ".htm");
            // line 28
            echo "                    ";
            $context['__cms_content_params'] = [];
            echo $this->env->getExtension('CMS')->contentFunction(($context["content"] ?? null)            , $context['__cms_content_params']            );
            unset($context['__cms_content_params']);
            // line 29
            echo "                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "            
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 31,  100 => 29,  95 => 28,  93 => 27,  90 => 26,  84 => 24,  82 => 23,  77 => 22,  75 => 21,  70 => 20,  68 => 19,  62 => 17,  60 => 16,  57 => 15,  53 => 14,  49 => 12,  46 => 11,  39 => 10,  37 => 9,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"bg-gray-lighter\" id=\"services\" data-target=\"#services\">
    <div class=\"container text-xs-center p-y-3\">
        <div class=\"row\">
            <div class=\"col-xs-12 text-xs-center\">
                <h2 class=\"m-b-1\">{{ headertext }}</h2>
                <p>{% content 'services/description.htm' %}</p>
            </div>
            <div class=\"col-xs-12\">
                {% if slideshow and slideshow.slideshow %}
                    {% partial 'slideshow' slideshow=slideshow.slideshow id='slideshow2' %}
                {% endif  %}
            </div>
            
            {% for i in 1..3 %}
                <div class=\"col-xs-12 col-md-4 p-t-2\">
                    {% set icon = 'assets/images/icons/' ~ this.theme.color ~ '/service_block_0' ~ i ~ '.svg' %}
                    <img class=\"center-block p-b-2\" src=\"{{ icon |theme }}\">
                    
                    {% if i == 1 %}
                        <h3 class=\"text-xs-center\">{{ headertextblock1 }}</h3>
                    {% elseif i == 2 %}
                        <h3 class=\"text-xs-center\">{{ headertextblock2 }}</h3>
                    {% elseif i == 3 %}
                        <h3 class=\"text-xs-center\">{{ headertextblock3 }}</h3>
                    {% endif %}
                    
                    {% set content = 'services/block' ~ i ~ '.htm' %}
                    {% content content %}
                </div>
            {% endfor %}
            
        </div>
    </div>
</div>", "E:\\xampp7\\htdocs\\october/themes/jumplink-viola/partials/services.htm", "");
    }
}
