<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/pages/products.htm */
class __TwigTemplate_9d5a1dfcba328931e553feeffd0cd1f33ec391e7a0f56a12218a3df0dc7d2562 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">Products</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                                <section class=\"contact-block shop container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t<header class=\"shop-header\">
\t\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h2>PRODUCT NAME</h2>
\t\t\t\t\t\t\t\t\t<p>Showing <a href=\"#\">1</a> - <a href=\"#\">9</a> of <a href=\"#\">83</a> Results</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"products-shop\">
\t\t\t\t\t\t\t\t<div class=\"products-holder side\">
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<footer class=\"shop-footer side\">
\t\t\t\t\t\t\t\t\t<ul class=\"shop-pagination list-inline\">
\t\t\t\t\t\t\t\t\t\t<li class=\"prev\"><a href=\"#\"><i class=\"fa fa-angle-double-left\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">1</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">2</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">3</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">4</a></li>
\t\t\t\t\t\t\t\t\t\t<li class=\"next\"><a href=\"#\"><i class=\"fa fa-angle-double-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<div class=\"txt-box\">
\t\t\t\t\t\t\t\t\t\t<p>1 - 9 of about 83 results found for <a href=\"#\">Product name</a></p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>";
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/products.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<main id=\"main\" role=\"main\">
\t\t\t\t<!-- shop gallery -->
                                
                                <header class=\"page-banner\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"container\">
\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h1 class=\"heading text-capitalize\">Products</h1>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</header>
                             
\t\t\t\t
                                <section class=\"contact-block shop container\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t<header class=\"shop-header\">
\t\t\t\t\t\t\t\t<div class=\"holder\">
\t\t\t\t\t\t\t\t\t<h2>PRODUCT NAME</h2>
\t\t\t\t\t\t\t\t\t<p>Showing <a href=\"#\">1</a> - <a href=\"#\">9</a> of <a href=\"#\">83</a> Results</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"products-shop\">
\t\t\t\t\t\t\t\t<div class=\"products-holder side\">
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t\t<!-- shop-product-info -->
\t\t\t\t\t\t\t\t\t<article class=\"shop-product-info\" data-delay=\"200\" data-animate=\"fadeInUp\">
\t\t\t\t\t\t\t\t\t\t<div class=\"product-img\">
\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" src=\"http://placehold.it/270x370\" alt=\"image description\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-f-default\">QUICK VIEW</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"produt-txt\">
\t\t\t\t\t\t\t\t\t\t\t<h2><a href=\"#\">Product Name</a></h2>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<p>Most text editors like MS Word or Lotus Notes generate random lorem text when needed, either as pre-installed module or plug-in to be added. Word selection or sequence don't necessarily match the original, which is intended to add variety. Presentation software like Keynote or Pages use it as a samples for screenplay layout.</p>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"buttons-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"btn-cart\">[ READ MORE ]</a>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<footer class=\"shop-footer side\">
\t\t\t\t\t\t\t\t\t<ul class=\"shop-pagination list-inline\">
\t\t\t\t\t\t\t\t\t\t<li class=\"prev\"><a href=\"#\"><i class=\"fa fa-angle-double-left\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">1</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">2</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">3</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">4</a></li>
\t\t\t\t\t\t\t\t\t\t<li class=\"next\"><a href=\"#\"><i class=\"fa fa-angle-double-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<div class=\"txt-box\">
\t\t\t\t\t\t\t\t\t\t<p>1 - 9 of about 83 results found for <a href=\"#\">Product name</a></p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t</main>", "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/products.htm", "");
    }
}
