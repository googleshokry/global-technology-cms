<?php

/* E:\xampp7\htdocs\october/themes/Global Technology/pages/index.htm */
class __TwigTemplate_48641c28ad3e44c2d96e32cf7663a66e99815d2f7b828e49b7cc1ca56334c1f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<main id=\"main\">

\t<!-- shop gallery -->

\t<div class=\"shop-gallery beans-slider\" data-rotate=\"true\">
\t
\t\t<div class=\"beans-mask\">
\t\t
\t\t\t<div class=\"beans-slideset\">
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1><span class=\"txt\">Canon</span> New</h1>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"shop-grid-4column.html\">VIEW new</a> <a class=\"btn btn-f-default\" href=\"shop-grid-3column-leftsidebar.html\">VIEW USED</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content add win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1>Canon</h1>

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">SHOP NOW</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content add2 win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1>Canon</h1>

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">SHOP NOW</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t
\t\t<div class=\"beans-pagination\">
\t\t
\t\t\t<br>
\t\t</div>
\t\t</div>

\t<!-- collection block -->

\t<div class=\"collection-block dark-bottom-border\">
\t
\t\t<div class=\"container\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<div class=\"col-sm-8 col-xs-12\">
\t\t\t\t
\t\t\t\t\t<!-- collection-col -->
\t\t\t\t
\t\t\t\t\t<div class=\"collection-col women\" data-animate=\"fadeInUp\" data-delay=\"300\"><img src=\"http://placehold.it/770x230\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t<div class=\"collection-over\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">WOMEN FASHION</span> COLLECTION</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">SHOP NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- collection-col -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x231\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t<div class=\"collection-over add\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">GET 10% OFF</span> MEN WEAR</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">FIND NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- collection-col -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x231\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t<div class=\"collection-over add\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">SPRING COLLECTION</span> FOR WOMEN</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">BUY NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<div class=\"col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t
\t\t\t\t\t<!-- collection-col -->
\t\t\t\t
\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x490\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t<div class=\"collection-over add2\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">THE 2015</span> COLLECTION</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">VIEW RANGE <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t<!-- product section -->

\t<section class=\"container product-section\">
\t
\t\t<!-- page heading -->
\t
\t\t<header class=\"page-heading small\">
\t\t
\t\t\t<div class=\"heading\">

\t\t\t

\t\t\t\t<h3 class=\"heading5 lime text-capitalize font-medium\">Latest Products</h3>
\t\t\t\t</div>
\t\t\t</header>
\t
\t\t<div class=\"row product-nav-holder\">
\t\t
\t\t\t<div class=\"col-xs-12\">

\t\t\t

\t\t\t\t<ul class=\"product-nav list-inline tabset\">
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab1\">category1</a></li>
\t\t\t\t
\t\t\t\t\t<li class=\"active\"><a href=\"#tab2\">categor2</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab3\">categ3</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab4\">categ4</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab5\">categ5</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t
\t\t<div class=\"row\">
\t\t
\t\t\t<div class=\"col-xs-12\">
\t\t\t
\t\t\t\t<!-- tab content -->
\t\t\t
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t
\t\t\t\t\t<!-- tab1 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab1\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab2 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab2\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab3 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab3\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab4 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab4\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab5 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab5\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOP</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>

\t<!-- special block -->

\t<div class=\"special-block\">
\t
\t\t<div class=\"parallax-holder\">
\t\t
\t\t\t<div class=\"parallax-frame\"><img src=\"http://placehold.it/1920x1000\" height=\"1000\" alt=\"image description\" style=\"width: 1178px;\" class=\"fr-dii fr-draggable\"></div>
\t\t\t</div>
\t
\t\t<div class=\"container\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<section class=\"col-xs-12 special-content\">
\t\t\t\t
\t\t\t\t\t<div class=\"special-box\">

\t\t\t\t\t

\t\t\t\t\t\t<h2>About us</h2>

\t\t\t\t\t

\t\t\t\t\t\t<p>Welcome to WordPress. This is your first post. Edit or delete it, then start bloggin Molestias excepturi sint occaecati cupiditate non provident similique sunt in culpa qui officia deserunt animi id est laborum et dolorum fuga et harum quidem rerum facilis est et expedita distinction.</p><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">Read more</a></div>
\t\t\t\t\t</section>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
</main>
";
        // line 1000
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("footer"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/index.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1020 => 1000,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<main id=\"main\">

\t<!-- shop gallery -->

\t<div class=\"shop-gallery beans-slider\" data-rotate=\"true\">
\t
\t\t<div class=\"beans-mask\">
\t\t
\t\t\t<div class=\"beans-slideset\">
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1><span class=\"txt\">Canon</span> New</h1>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"shop-grid-4column.html\">VIEW new</a> <a class=\"btn btn-f-default\" href=\"shop-grid-3column-leftsidebar.html\">VIEW USED</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content add win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1>Canon</h1>

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">SHOP NOW</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<!-- beans-slide -->
\t\t\t
\t\t\t\t<div class=\"beans-slide\">
\t\t\t\t
\t\t\t\t\t<div class=\"stretch\"><img src=\"http://placehold.it/1920x1000\" alt=\"image description\" class=\"fr-dii fr-draggable\"></div>
\t\t\t\t
\t\t\t\t\t<section class=\"container\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-12 shop-content add2 win-height\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<header class=\"shop-heading\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h1>Canon</h1>

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<p>Suspendisse et metus eu massa lobortis condimentum sed ut orci. Nullam viverra dapibus risus, eu tristique nisl sollicitudin at. Etiam iaculis blandit libero.</p>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"button-slider\"><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">SHOP NOW</a></div>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t
\t\t<div class=\"beans-pagination\">
\t\t
\t\t\t<br>
\t\t</div>
\t\t</div>

\t<!-- collection block -->

\t<div class=\"collection-block dark-bottom-border\">
\t
\t\t<div class=\"container\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<div class=\"col-sm-8 col-xs-12\">
\t\t\t\t
\t\t\t\t\t<!-- collection-col -->
\t\t\t\t
\t\t\t\t\t<div class=\"collection-col women\" data-animate=\"fadeInUp\" data-delay=\"300\"><img src=\"http://placehold.it/770x230\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t<div class=\"collection-over\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">WOMEN FASHION</span> COLLECTION</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">SHOP NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- collection-col -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x231\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t<div class=\"collection-over add\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">GET 10% OFF</span> MEN WEAR</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">FIND NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- collection-col -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x231\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t<div class=\"collection-over add\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">SPRING COLLECTION</span> FOR WOMEN</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">BUY NOW <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t<div class=\"col-sm-4 col-xs-12\" data-animate=\"fadeInUp\" data-delay=\"300\">
\t\t\t\t
\t\t\t\t\t<!-- collection-col -->
\t\t\t\t
\t\t\t\t\t<div class=\"collection-col\"><img src=\"http://placehold.it/370x490\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t<div class=\"collection-over add2\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"box\">

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t<h2><span class=\"txt\">THE 2015</span> COLLECTION</h2><a class=\"btn-shop\" href=\"shop-checkout.html\">VIEW RANGE <i class=\"fa fa-angle-right\"></i></a></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t<!-- product section -->

\t<section class=\"container product-section\">
\t
\t\t<!-- page heading -->
\t
\t\t<header class=\"page-heading small\">
\t\t
\t\t\t<div class=\"heading\">

\t\t\t

\t\t\t\t<h3 class=\"heading5 lime text-capitalize font-medium\">Latest Products</h3>
\t\t\t\t</div>
\t\t\t</header>
\t
\t\t<div class=\"row product-nav-holder\">
\t\t
\t\t\t<div class=\"col-xs-12\">

\t\t\t

\t\t\t\t<ul class=\"product-nav list-inline tabset\">
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab1\">category1</a></li>
\t\t\t\t
\t\t\t\t\t<li class=\"active\"><a href=\"#tab2\">categor2</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab3\">categ3</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab4\">categ4</a></li>
\t\t\t\t
\t\t\t\t\t<li><a href=\"#tab5\">categ5</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t
\t\t<div class=\"row\">
\t\t
\t\t\t<div class=\"col-xs-12\">
\t\t\t
\t\t\t\t<!-- tab content -->
\t\t\t
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t
\t\t\t\t\t<!-- tab1 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab1\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab2 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab2\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab3 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab3\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab4 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab4\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<!-- tab5 -->
\t\t\t\t
\t\t\t\t\t<div id=\"tab5\">
\t\t\t\t\t
\t\t\t\t\t\t<!-- products-holder -->
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"products-holder\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOP</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- new-product -->
\t\t\t\t\t\t
\t\t\t\t\t\t\t<article class=\"new-product\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"product-img\"><img src=\"http://placehold.it/270x370\" alt=\"image description\" class=\"fr-dii fr-draggable\">
\t\t\t\t\t\t\t\t\t<div class=\"product-over\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"frame\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"box\"><a class=\"btn lightbox btn-f-default\" href=\"images/placeholder.jpg\">QUICK VIEW</a></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div><span class=\"title\">TOPS</span>

\t\t\t\t\t\t\t\t<h2><a href=\"shop-single.html\">Product Name</a></h2>
\t\t\t\t\t\t\t\t</article>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</section>

\t<!-- special block -->

\t<div class=\"special-block\">
\t
\t\t<div class=\"parallax-holder\">
\t\t
\t\t\t<div class=\"parallax-frame\"><img src=\"http://placehold.it/1920x1000\" height=\"1000\" alt=\"image description\" style=\"width: 1178px;\" class=\"fr-dii fr-draggable\"></div>
\t\t\t</div>
\t
\t\t<div class=\"container\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t
\t\t\t\t<section class=\"col-xs-12 special-content\">
\t\t\t\t
\t\t\t\t\t<div class=\"special-box\">

\t\t\t\t\t

\t\t\t\t\t\t<h2>About us</h2>

\t\t\t\t\t

\t\t\t\t\t\t<p>Welcome to WordPress. This is your first post. Edit or delete it, then start bloggin Molestias excepturi sint occaecati cupiditate non provident similique sunt in culpa qui officia deserunt animi id est laborum et dolorum fuga et harum quidem rerum facilis est et expedita distinction.</p><a class=\"btn btn-f-default\" href=\"http://fekra.csshunter.com\">Read more</a></div>
\t\t\t\t\t</section>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
</main>
{% partial \"footer\" %}", "E:\\xampp7\\htdocs\\october/themes/Global Technology/pages/index.htm", "");
    }
}
